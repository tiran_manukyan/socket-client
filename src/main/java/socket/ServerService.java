package socket;

import module.ActionType;
import ui.ServerDataController;
import util.PropertyManager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class ServerService extends Thread {

    private Socket socket;

    private ObjectOutputStream writer;

    private String clientName;

    private ServerDataController dataController;

    private boolean running = true;

    @SuppressWarnings("unchecked")
    @Override
    public void run() {

        try {
            writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());

            sendClientName();
            dataWriter();

            while (running) {
                TransferObject readObject = (TransferObject) reader.readObject();

                switch (readObject.getActionType()) {

                    case DATA_LIST:
                        dataController.dataInput((List<ClientData>) readObject.getData());
                        break;
                    case CLOSE:
                        closeSocketByServer();
                }
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ServerService(Runnable isSucceeded, Runnable isFailed, String clientName) {
        this.clientName = clientName;
        try {
            onSuccessConnect(new Socket(PropertyManager.getInstance().getIpAddress(), Integer.parseInt(PropertyManager.getInstance().getPort())));
            isSucceeded.run();
            setDaemon(true);
            start();
        } catch (IOException e) {
            isFailed.run();
        }
    }

    private void sendClientName() {
        try {
            TransferObject<String> transferObject = new TransferObject<>();
            transferObject.setData(clientName);
            transferObject.setActionType(ActionType.CLIENT_NAME);
            writer.writeObject(transferObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void dataWriter() {
        dataController.onSendAction(value -> {
            try {
                TransferObject<Long> transferObject = new TransferObject<>();
                transferObject.setData(value);
                transferObject.setActionType(ActionType.DATA_INPUT);
                writer.writeObject(transferObject);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void closeSocketByClient() {
        if (socket != null && !socket.isClosed()) {
            try {
                TransferObject transferObject = new TransferObject();
                transferObject.setActionType(ActionType.CLOSE);
                writer.writeObject(transferObject);
            } catch (IOException e) {
                e.printStackTrace();
            }
            running = false;
        }
    }

    private void closeSocketByServer() throws IOException {
        if (!socket.isClosed()) {
            running = false;
            dataController.socketClosed();
            socket.close();
        }
    }

    private void onSuccessConnect(Socket socket) {
        this.socket = socket;
        this.dataController = new ServerDataController(event -> onGetDataAction());
        dataController.onStageClose(this::closeSocketByClient);
    }

    private void onGetDataAction() {
        try {
            TransferObject transferObject = new TransferObject();
            transferObject.setActionType(dataController.getDataType());
            writer.writeObject(transferObject);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
