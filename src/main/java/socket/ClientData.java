package socket;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ClientData implements Serializable {
    private static final long serialVersionUID = 5158569205227845848L;

    private int id;

    private long number;

    @Override
    public String toString() {
        return String.valueOf(number);
    }
}
