package module;

public enum NumberType {

    ODD_NUMBERS("Getting odd numbers"), EVEN_NUMBERS("Getting even numbers");

    String text;

    NumberType(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
