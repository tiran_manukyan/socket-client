package ui;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import socket.ClientData;
import module.NumberType;

import java.util.List;

class DataView extends AnchorPane {

    private ListView<ClientData> listView;

    private ChoiceBox<NumberType> choiceReceivedData;

    private Button btnGetData;

    DataView(EventHandler<ActionEvent> onGetDataAction) {
        createListView();
        createChoiceBox();
        createButtonGetData(onGetDataAction);
        getChildren().addAll(listView, btnGetData, choiceReceivedData);
    }

    private void createListView() {
        listView = new ListView<>();
        listView.setStyle("-fx-font-size: 16");

        AnchorPane.setTopAnchor(listView, 40.0);
        AnchorPane.setLeftAnchor(listView, 0.0);
        AnchorPane.setRightAnchor(listView, 0.0);
        AnchorPane.setBottomAnchor(listView, 0.0);
    }

    private void createChoiceBox() {
        choiceReceivedData = new ChoiceBox<>();

        choiceReceivedData.setLayoutX(100);
        choiceReceivedData.setLayoutY(10);

        choiceReceivedData.getItems().addAll(NumberType.EVEN_NUMBERS, NumberType.ODD_NUMBERS);

        choiceReceivedData.getSelectionModel().selectFirst();
    }

    void socketClosed() {
        btnGetData.setDisable(true);
        choiceReceivedData.setDisable(true);
    }

    private void createButtonGetData(EventHandler<ActionEvent> onGetDataAction) {
        btnGetData = new Button("Get data");

        btnGetData.setLayoutX(10);
        btnGetData.setLayoutY(10);

        btnGetData.setOnAction(onGetDataAction);
    }

    void inputData(List<ClientData> dataList) {
        listView.setItems(FXCollections.observableArrayList(dataList));
    }

    NumberType getSelectionNumberType() {
        return choiceReceivedData.getSelectionModel().getSelectedItem();
    }
}
