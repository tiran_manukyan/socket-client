package ui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.Getter;
import socket.ServerService;
import util.PropertyManager;


class ConnectionController {

    @Getter
    private AnchorPane anchorPane;

    private TextField txtClientName;

    private Button btnConnect;

    private Stage stage;

    private CustomButton btnEditProperties;

    private Label lblInfo;

    ConnectionController() {
        createConnectButton();
        createTextFieldClientName();
        createEditPropertiesButton();
        createLabelInfo();
        createAnchorPane();
        createStage();
        PropertyManager.getInstance().readProperties();
    }

    private void createAnchorPane() {
        anchorPane = new AnchorPane();
        anchorPane.getChildren().addAll(txtClientName, btnConnect, lblInfo, btnEditProperties);
    }

    private void createTextFieldClientName() {
        txtClientName = new TextField();
        txtClientName.setPromptText("Client name");
        txtClientName.setFocusTraversable(false);
        txtClientName.setFont(Font.font(15));
        txtClientName.setLayoutX(20);
        txtClientName.setLayoutY(10);
        txtClientName.setPrefWidth(165);
    }

    private void createLabelInfo() {
        lblInfo = new Label("Server is not responding");
        lblInfo.setTextFill(Color.RED);
        lblInfo.setFont(Font.font(14.5));
        lblInfo.setLayoutX(8);
        lblInfo.setLayoutY(88);
        lblInfo.setVisible(false);
    }

    private void createConnectButton() {
        btnConnect = new Button("Connect");
        btnConnect.setDefaultButton(true);
        btnConnect.setFont(Font.font(14.5));

        btnConnect.setLayoutX(20);
        btnConnect.setLayoutY(50);
        btnConnect.setPrefWidth(165);

        btnConnect.setOnAction(event -> {

            String value = txtClientName.getText();
            if (value.isEmpty()) {
                txtClientName.setStyle("-fx-border-color: red");
            } else {
                txtClientName.setStyle(null);
                reconnect();
                new ServerService(this::onSuccess, this::connectionFailed, value);
            }
        });
    }

    private void createEditPropertiesButton() {
        btnEditProperties = new CustomButton();
        btnEditProperties.setImage("/image/edit.png");
        btnEditProperties.setLayoutX(162);
        btnEditProperties.setLayoutY(80);
        btnEditProperties.setVisible(false);
        btnEditProperties.setTooltip("Edit properties");

        btnEditProperties.setOnAction(event -> PropertyManager.getInstance().openPropertyFile());
    }

    private void onSuccess() {
        stage.close();
    }

    private void connectionFailed() {
        stage.setHeight(145);
        btnEditProperties.setVisible(true);
        lblInfo.setVisible(true);
    }

    private void reconnect() {
        stage.setHeight(120);
        lblInfo.setVisible(false);
        btnEditProperties.setVisible(false);
        PropertyManager.getInstance().readProperties();
    }

    private void createStage() {
        stage = new Stage();
        stage.setWidth(210);
        stage.setHeight(120);
        Parent root = anchorPane;
        root.getStylesheets().add(getClass().getResource("/css/style.css").toExternalForm());
        stage.setScene(new Scene(root));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setTitle("Connecting to socket");
        stage.show();
    }
}
