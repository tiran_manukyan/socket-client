package ui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import module.ActionType;
import module.NumberType;
import socket.ClientData;

import java.util.List;
import java.util.function.Consumer;

public class ServerDataController {

    private TabPane tabPane;

    private Tab tabGetData;

    private Tab tabSendData;

    private AnchorPane paneSendData;

    private TextField txtInputData;

    private Button btnSendData;

    private Stage stage;

    private DataView dataView;

    public ServerDataController(EventHandler<ActionEvent> onGetDataAction) {
        createButtonSendData();
        createTextField();
        createPaneSendData();
        createDataView(onGetDataAction);
        createTabGetData();
        createTabSendData();
        createTabPane();
        createStage();
    }

    private void createTabPane() {
        tabPane = new TabPane();
        tabPane.getTabs().addAll(tabGetData, tabSendData);
    }

    private void createTabGetData() {
        tabGetData = new Tab("Get data from server");
        tabGetData.setContent(dataView);
        tabGetData.setClosable(false);
    }

    private void createTabSendData() {
        tabSendData = new Tab("Send data to server");
        tabSendData.setContent(paneSendData);
        tabSendData.setClosable(false);
    }

    private void createPaneSendData() {
        paneSendData = new AnchorPane();
        paneSendData.getChildren().addAll(txtInputData, btnSendData);
    }

    private void createButtonSendData() {
        btnSendData = new Button("Send data (only digits)");

        btnSendData.setFont(Font.font(16));
        btnSendData.setDefaultButton(true);

        AnchorPane.setTopAnchor(btnSendData, 70.0);
        AnchorPane.setLeftAnchor(btnSendData, 20.0);
        AnchorPane.setRightAnchor(btnSendData, 20.0);

    }

    private void createTextField() {
        txtInputData = new TextField();

        txtInputData.setFont(Font.font(16));

        AnchorPane.setTopAnchor(txtInputData, 20.0);
        AnchorPane.setLeftAnchor(txtInputData, 20.0);
        AnchorPane.setRightAnchor(txtInputData, 20.0);

        txtInputData.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.chars().anyMatch(value -> !Character.isDigit(value))) {
                txtInputData.setText(oldValue);
            }
        });
    }

    private void createDataView(EventHandler<ActionEvent> onGetDataAction) {
        dataView = new DataView(onGetDataAction);
    }

    public void socketClosed() {
        Platform.runLater(() -> {
            tabSendData.setDisable(true);
            dataView.socketClosed();
            stage.setTitle("Socket closed by server");
        });

    }

    public void onSendAction(Consumer<Long> provider) {
        btnSendData.setOnAction(event -> {
            try {
                long value = Long.parseLong(txtInputData.getText());
                txtInputData.setStyle(null);
                provider.accept(value);
            } catch (NumberFormatException ex) {
                txtInputData.setStyle("-fx-border-color: red");
            }
        });
    }

    public void dataInput(List<ClientData> clientData) {
        Platform.runLater(() -> dataView.inputData(clientData));
    }

    private void createStage() {
        stage = new Stage();
        stage.setHeight(240);
        stage.setMinHeight(240);
        stage.setMaxHeight(720);
        stage.setMaxWidth(310);
        stage.setMinWidth(310);
        stage.setScene(new Scene(tabPane));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Read and send data");
        stage.show();
    }

    public ActionType getDataType() {
        if (dataView.getSelectionNumberType() == NumberType.EVEN_NUMBERS) {
            return ActionType.LIST_OF_EVEN_NUMBERS;
        } else {
            return ActionType.LIST_OF_ODD_NUMBERS;
        }
    }

    public void onStageClose(Runnable runnable) {
        stage.setOnCloseRequest(event -> runnable.run());
    }
}
